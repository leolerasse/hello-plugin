/**
 *  @file USDLRecognizer.h
 *
 * Copyright (c)2018 Microblink Ltd. All rights reserved.
 *
 * ANY UNAUTHORIZED USE OR SALE, DUPLICATION, OR DISTRIBUTION
 * OF THIS PROGRAM OR ANY OF ITS PARTS, IN SOURCE OR BINARY FORMS,
 * WITH OR WITHOUT MODIFICATION, WITH THE PURPOSE OF ACQUIRING
 * UNLAWFUL MATERIAL OR ANY OTHER BENEFIT IS PROHIBITED!
 * THIS PROGRAM IS PROTECTED BY COPYRIGHT LAWS AND YOU MAY NOT
 * REVERSE ENGINEER, DECOMPILE, OR DISASSEMBLE IT.
 */

#ifndef USDL_RECOGNIZER_H_INCLUDED
#define USDL_RECOGNIZER_H_INCLUDED

#include "Recognizer/Export.h"
#include "Recognizer/RecognizerError.h"
#include "Recognizer/Types.h"

#include "Recognizer/BlinkBarcode/BarcodeData.h"
#include "Recognizer/Recognizer.h"

#include "USDLKeys.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @struct MBUsdlRecognizerSettings
 * @brief Settings for configuring MBUsdlRecognizer
 */
struct MBUsdlRecognizerSettings
{
    /**
     * Allow scanning barcodes which don't have quiet zone surrounding it (e.g. text
     * concatenated with barcode). This option can drastically increase recognition time and
     * can even reduce the recognition quality of barcodes that do have quiet zone
     * surrounding it.
     */
    MBBool nullQuietZoneAllowed;

    /**
     * Allow scanning of uncertain PDF417 barcodes. Uncertain scanning refers to returning result even on barcodes
     * which checksum does not match. Ideal for incomplete or damaged barcodes.
     */
    MBBool shouldScanUncertain;

#ifdef __cplusplus
    /**
     * Default constructor for c++.
     */
    MBUsdlRecognizerSettings() :
        nullQuietZoneAllowed( MB_FALSE ),
        shouldScanUncertain ( MB_FALSE )
    {}
#endif
};

/**
 * @brief Typedef for the MBUsdlRecognizerSettings structure.
 */
MB_EXPORTED_TYPE typedef struct MBUsdlRecognizerSettings MBUsdlRecognizerSettings;


/**
 * @struct MBUsdlRecognizerResult
 * @brief A result that can be obtained from MBUsdlRecognizer.
 */
struct MBUsdlRecognizerResult
{
    /** Base result common to all recognizer results. */
    MBBaseRecognizerResult baseResult;

    /** This structure contains all scanned data from the barcode */
    MBBarcodeData barcodeData;

    /**
     * Contains all fields extracted from the US Driver's License.
     * Fields that are empty will point to empty string.
     * Pointers are valid as long as owning MBUsdlRecognizer is not
     * destroyed and is not used for performing the new recognition.
     */
    char const * fields[ USDL_KEY_KeysSize ];

    /**
     * Contains all fields that are specific to state in which
     * US Driver's License was issued and do not exist in
     * fields array. Maximum supported number of state-specific
     * fields is 10.
     */
    char const * dynamicFields[ 10 ];

    /**
     * Number of elements in dynamicFields.
     */
    size_t dynamicFieldSize;
};

/**
 * @brief Typedef for the MBUsdlRecognizerResult structure.
 */
MB_EXPORTED_TYPE typedef struct MBUsdlRecognizerResult MBUsdlRecognizerResult;

/**
 * @struct MBUsdlRecognizer
 * @brief A recognizer that can scan and parse 2D barcodes from back side of US Driver's Licenses.
 */
struct MBUsdlRecognizer;

/**
 * @brief Typedef for the MBUsdlRecognizer structure.
 */
MB_EXPORTED_TYPE typedef struct MBUsdlRecognizer MBUsdlRecognizer;

/**
 * @memberof MBUsdlRecognizer
 * @brief Allocates and initializes new MBUsdlRecognizer object.
 * @param usdlRecognizer Pointer to pointer referencing the created MBUsdlRecognizer object.
 * @param usdlRecognizerSettings Settings that will be used for creating of the MBUsdlRecognizer object.
 * @return status of the operation. The operation may fail (i.e. if license key is not set or does not allow
 *         usage of the requested recognizer), so please check the returned status for possible errors.
 */
MB_API MBRecognizerErrorStatus MB_CALL usdlRecognizerCreate( MBUsdlRecognizer ** usdlRecognizer, MBUsdlRecognizerSettings const * usdlRecognizerSettings );

/**
 * @memberof MBUsdlRecognizer
 * @brief Updates the MBUsdlRecognizer with the new settings.
 * Note that updating the recognizer while it is being in use by MBRecognizerRunner will fail.
 * @param usdlRecognizer MBUsdlRecognizer that should be updated with new settings.
 * @param usdlRecognizerSettings Settings that will be used for updating the MBUsdlRecognizer object.
 * @return status of the operation. The operation may fail (i.e. if recognizer is in use by the MBRecognizerRunner),
 *         so please check the returned status for possible errors.
 */
MB_API MBRecognizerErrorStatus MB_CALL usdlRecognizerUpdate( MBUsdlRecognizer * usdlRecognizer, MBUsdlRecognizerSettings const * usdlRecognizerSettings );

/**
 * @memberof MBUsdlRecognizer
 * @brief Obtains the result from the given MBUsdlRecognizer object.
 * @param result MBUsdlRecognizerResult structure that will be filled with the recognized data.
 * Note that all pointers in structure will remain valid until given recognizer is
 * destroyed with usdlRecognizerDelete function or is used for performing the new recognition.
 * @param usdlRecognizer MBUsdlRecognizer from which result should be obtained.
 * @return status of the operation.
 */
MB_API MBRecognizerErrorStatus MB_CALL usdlRecognizerResult( MBUsdlRecognizerResult * result, MBUsdlRecognizer const * usdlRecognizer );

/**
 * @memberof MBUsdlRecognizer
 * @brief Destroys the given MBUsdlRecognizer.
 * @param usdlRecognizer Pointer to pointer to MBUsdlRecognizer structure that needs to be destroyed.
 * After destruction, the pointer to MBUsdlRecognizer structure will be set to NULL.
 * @return status of the operation
 */
MB_API MBRecognizerErrorStatus MB_CALL usdlRecognizerDelete( MBUsdlRecognizer ** usdlRecognizer );

#ifdef __cplusplus
}
#endif

#endif
