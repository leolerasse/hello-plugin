//
//  IDSCore.h
//  MJCS SDK
//
//  Created by Stanislav Kozyrev <s.kozyrev@idscan.co.uk>.
//  Copyright (c) 2014 IDScan Biometrics Ltd. All rights reserved.
//

#import <MJCS/IDSBase.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface IDSCore : NSObject

+ (NSBundle *)bundle;

+ (NSString *)pathForResource:(NSString *)name ofType:(NSString *)ext;

+ (NSString *)pathForResource:(NSString *)name ofType:(NSString *)ext inDirectory:(NSString *)subpath;

+ (NSString *)pathForResource:(NSString *)name ofType:(NSString *)ext inDirectory:(NSString *)subpath forLocalization:(NSString *)localizationName;

+ (void)mjcs_log:(NSString *)logString;

+ (BOOL)isWhitelabelEnabled;

+ (UIImage *)loadImage:(NSString *)imageFileName;
@end
