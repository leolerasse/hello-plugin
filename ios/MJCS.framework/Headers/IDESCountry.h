//
//  IDESCountry.h
//  idesv2
//
//  Created by Abdulrhman Babelli on 9/5/18.
//  Copyright © 2018 GB Group Plc All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Interface, which represents a country in the profile
 */
@interface IDESCountry : NSObject

/**
 ISO code of the country
 */
@property (nonatomic, readonly, nonnull) NSString *code;

/**
 Name of the country
 */
@property (nonatomic, readonly, nonnull) NSString *name;

/**
 Supported documents in the current profile
 */
@property (nonatomic, readonly, nonnull) NSArray<NSString *> *documents;

@end
