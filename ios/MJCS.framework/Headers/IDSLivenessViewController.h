//
//  IDSLivenessViewController.h
//  MJCS
//
//  Created by Edvardas Maslauskas on 01/06/2018.
//  Copyright © 2018 IDScan Biometrics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IDSLivenessOverlayProtocol.h"

@class IDSLivenessViewController;
@class IDSLivenessResult;
@class IDSLivenessConfig;

__attribute__((deprecated("IDSLivenessViewController is deprecated since 9.0.0 version. Please use IDSLivenessOfflineViewController or IDSLivenessOnlineViewController")))
@protocol IDSLivenessDelegate <NSObject>

@required
/**
 Delegate method, which will be called once liveness test is finished

 @param livenessController the instance of liveness controller
 @param result IDSLivenessResult, which contains high level outcome and all performed actions
 */
- (void)didFinishLiveness:(nonnull IDSLivenessViewController *)livenessController
               withResult:(nonnull IDSLivenessResult *)result;

@required
/**
 Delegate method, which could return errors while performing liveness

 @param livenessController the instance of liveness controller
 @param error NSError describing the failure
 */
- (void)didFailLiveness:(nonnull IDSLivenessViewController *)livenessController
              withError:(nonnull NSError *)error;

@optional
/**
 Delegate method, which will pass face images while performing liveness detection.
 It could be used to do the face match in the meantime while doing liveness. The number of calls will depend on the liveness engine configuration
 @see IDSLivenessConfig numberOfFaceImages

 @param image of the person looking straight into the camera
 */
- (void)receivedFaceImage:(nonnull UIImage *)image;

@end

__attribute__((deprecated("IDSLivenessViewController is deprecated since 9.0.0 version. Please use IDSLivenessOfflineViewController or IDSLivenessOnlineViewController")))
@interface IDSLivenessViewController : UIViewController

/**
 Delegete subsriber, which will receive callbacks
 */
@property (nonatomic, weak, nullable) id<IDSLivenessDelegate> delegate;

/**
 Main constructor for liveness controller

 @param config Liveness configuration
 */
- (instancetype _Nonnull )initWithConfig:(IDSLivenessConfig *_Nonnull)config;

/**
 Main constructor for liveness controller with the ability to pass custom overlay

 @param config Liveness configuration
 @param controller custom overay, which should conform to the protocol
 */
- (instancetype _Nonnull )initWithConfig:(IDSLivenessConfig *_Nonnull)config overlayController:(UIViewController <IDSLivenessOverlayProtocol> *_Nonnull)controller;

/**
 Method, which should be call to start the actual liveness test
 @warning use this method only when passing custom overlay!
 @discussion as it takes a bit of time until liveness is loaded, ideally we want to display customer some loading screen and
 start the test when user either clicks to the button to proceed or we can do this automatically
 */
- (void)startLiveness;
@end
