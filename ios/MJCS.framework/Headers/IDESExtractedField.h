//
//  IDESExtractedField.h
//  idesv2
//
//  Created by David Okun on 06/07/2016.
//  Copyright © 2016 IDscan Biometrics Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CGGeometry.h>

/**
 * Returns the format that was used to standardize or normalize the field value.
 * @return format of extracted field value.
 */
typedef NS_ENUM(NSInteger, IDESFieldValueFormat) {
    /**
     *  No specified format
     */
    IDESFieldValueFormatUndefined = 0,
    /**
     *  YYYY-MM-DD
     */
    IDESFieldValueFormatDateISO8601 = 1,
    /**
     *  YYYY-MM-DDTHH:mm:ss+ZZ:ZZ
     */
    IDESFieldValueFormatDateTimeISO8601 = 2,
    /**
     *  Three-letter country codes
     */
    IDESFieldValueFormatCountryCodeISO316613Alpha = 3,
    /**
     *  Country name
     */
    IDESFieldValueFormatCountryName = 4,
    /**
     *  Personal name
     */
    IDESFieldValueFormatPersonalName = 6,
    /**
     *  Gender if available, standardized to Male, Female, Other, or Unknown
     */
    IDESFieldValueFormatGender = 10,
    /**
     *  Data in MRZ in pre-defined format
     */
    IDESFieldValueFormatMrzData = 12
};

/**
 *  Object that represents an extracted field of text on a document as returned from the IDESDocumentProcessingService
 */
@interface IDESExtractedField : NSObject

/**
 * Returns the extracted field name which is used to identify the field.
 * Field names follow an IDES naming standard
 * @return name of extracted field.
 */
@property (nonatomic, readonly, nonnull) NSString *name;

/**
 * Returns the normalized value of the extracted field represented as string.
 * The value is standardized or normalized according to the format specified
 * in `[IDESExtractedField format]` property.
 * @return value of the extracted field.
 */
@property (nonatomic, readonly, nonnull) NSString *value;

/**
 * Returns the normalized local value of the extracted field represented as string.
 * The value is returned in same language that is used on the original document, for example Chinese
 * @return localised value of the extracted field.
 */
@property (nonatomic, readonly, nullable) NSString *localValue;

/**
 @return boolean value whether field contains local value
 */
@property (assign, readonly) BOOL hasLocalValue;

/**
 * Returns the format that was used to standardize or normalize the field value.
 * @return format of extracted field value.
 */
@property (nonatomic, readonly) IDESFieldValueFormat format;

/**
 * Returns the description of the extracted field. Which is a friendly text
 * describing the content or the purpose of the field.
 * @return description of the extracted field.
 */
@property (nonatomic, readonly, nonnull) NSString *fieldDescription;

/**
 * Returns list of regions in document image where field value was read.
 * @return array of regions in document image. Region is neturned as CGRect wrapped to NSValue
 */
@property (nonatomic, readonly, nonnull) NSArray<NSValue *> *regions;

@end
