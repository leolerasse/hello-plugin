#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint flgbgidscan.podspec' to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'flgbgidscan'
  s.version          = '0.0.1'
  s.summary          = 'A new flutter plugin project.'
  s.description      = <<-DESC
A new flutter plugin project.
                       DESC
  s.homepage         = 'http://example.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Your Company' => 'email@example.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*', 'IDES-default-profile.zip'
  s.dependency 'Flutter'
  s.vendored_frameworks = 'MJCS.framework', "ZipZap.framework", "RecognizerApi.framework", "FLAnimatedImage.framework", "IDSLiveness.framework", "ReadID_UI.framework", "ReadID.framework"
  s.platform = :ios, '11.0'
  # Flutter.framework does not contain a i386 slice. Only x86_64 simulators are supported.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'VALID_ARCHS[sdk=iphonesimulator*]' => 'x86_64' }
  s.swift_version = '5.0'
end
