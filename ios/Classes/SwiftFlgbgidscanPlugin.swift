import Flutter
import UIKit
import MJCS
public class SwiftFlgbgidscanPlugin: NSObject, FlutterPlugin {
    
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "flgbgidscan", binaryMessenger: registrar.messenger())
        let instance = SwiftFlgbgidscanPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        if(call.method == "startUserJourney"){
            MJCS.loadSDK()
            DispatchQueue.main.async {
                let spvc = SwiftKycPluginViewController.init()
                spvc.flutterResult = result
                spvc.startKYC(call: call, result: result)
            }
            
        }else{
            result(FlutterMethodNotImplemented)
        }
    }
}

class SwiftKycPluginViewController: UIViewController {
    var journeyResponse: IDSEnterpriseJourneyResponse?
    var flutterResult: FlutterResult?
    
    func startKYC(call: FlutterMethodCall, result: FlutterResult) {
        
        if let args = call.arguments as? Dictionary<String, Any>,
            let baseUrl = args["baseUrl"] as? String,
            let username = args["username"] as? String,
            let password = args["password"] as? String
        {
            let credentials = IDSEnterpriseCredentials.init(username: username, password: password, urlPrefix: baseUrl)
            let customerController = IDSCustomerJourneyController.init()
            customerController.credentials = credentials
            customerController.delegate = self
            UIApplication.shared.keyWindow?.rootViewController?.present(customerController, animated: true, completion: nil)
            
        } else {
            result(FlutterError.init(code: "bad args", message: nil, details: nil))
        }
    }
}

extension SwiftKycPluginViewController: IDSCustomerJourneyControllerDelegate {
    func customerJourneyController(_ scanner: IDSCustomerJourneyController, didFinishJourneyWithResult result: IDSEnterpriseJourneyResponse?) {
        self.journeyResponse = result
        dismiss(animated: true, completion: nil)
        if let safeRes = flutterResult {
            safeRes(result?.journeyID)
        }
    }
    
    func customerJourneyController(_ scanner: IDSCustomerJourneyController, didFailWithError error: Error?) {
        dismiss(animated: true, completion: nil)
        if let safeRes = flutterResult {
            safeRes(FlutterError(code: error?.localizedDescription ?? "Unknown Error", message: nil, details: nil))
        }
    }
    
    func customerJourneyControllerDidCancel(_ scanner: IDSCustomerJourneyController) {
        dismiss(animated: true, completion: nil)
        if let safeRes = flutterResult {
            safeRes(FlutterError(code: "User canceled", message: nil, details: nil))
        }
    }
}
