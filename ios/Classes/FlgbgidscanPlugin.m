#import "FlgbgidscanPlugin.h"
#if __has_include(<flgbgidscan/flgbgidscan-Swift.h>)
#import <flgbgidscan/flgbgidscan-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flgbgidscan-Swift.h"
#endif

@implementation FlgbgidscanPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlgbgidscanPlugin registerWithRegistrar:registrar];
}
@end
