//
//  IDSLivenessAction.h
//  IDSLiveness
//
//  Created by Edvardas Maslauskas on 24/04/2018.
//  Copyright © 2018 GB Group plc ('GBG'). All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 Enum values with possible actions states that can be requested for liveness

 - IDSLivenessActionStateSmile: Liveness action state for smile
 - IDSLivenessActionStateFrown: Liveness action state for frown
 - IDSLivenessActionStateTiltLeft: Liveness action state to tilt head left
 - IDSLivenessActionStateTiltRight: Liveness action state to tilt head right
 - IDSLivenessActionStateTiltUp: Liveness action state to tilt head up
 - IDSLivenessActionStateTiltDown: Liveness action state to tilt head down
 - IDSLivenessActionStateTiltStraight: Liveness action state to look straight
 - IDSLivenessActionStateNoAction: No liveness action required
 */
typedef NS_ENUM(NSInteger, IDSLivenessAction) {
    IDSLivenessActionSmile = 0,
    IDSLivenessActionFrown = 1,
    IDSLivenessActionTiltLeft = 2,
    IDSLivenessActionTiltRight = 3,
    IDSLivenessActionTiltUp = 4,
    IDSLivenessActionTiltDown = 5,
    IDSLivenessActionTiltStraight = 6,
    IDSLivenessActionNoAction = 7
};

/**
 Enum depicting the outcome of liveness action
 - IDSLivenessActionResultStateFailed: Action has failed
 - IDSLivenessActionResultStatePassed: Action successfuly passed
 - IDSLivenessActionResultStateTimeout: Action was not perfomed in specified timeframe and failed
 - IDSLivenessActionResultStateInterrupted: Action was interrupted while performing it
 - IDSLivenessActionResultStateUndefined: Action was never asked to be performed
 */
typedef NS_ENUM(NSInteger, IDSLivenessActionResult) {
    IDSLivenessActionResultFailed = 0,
    IDSLivenessActionResultPassed = 1,
    IDSLivenessActionResultTimeout = 2,
    IDSLivenessActionResultInterrupted = 3,
    IDSLivenessActionResultUndefined = 4
};

@interface IDSLivenessActionState : NSObject

/**
 Returns enum value of liveness action
 */
@property (nonatomic, readonly) IDSLivenessAction action;

/**
 Returns state of liveness action
 */
@property (nonatomic, readonly) IDSLivenessActionResult actionState;

/**
 Return a time in ms how long did it take top perform an action
 */
@property (nonatomic, readonly, nonnull) NSNumber *timeSinceStart;

/**
 Returns human readable description of liveness action
 */
@property (nonatomic, readonly, nonnull) NSString *actionDescription;

/**
 Returns the last frame of the image while performing an action
 */
@property (nonatomic, readonly, nonnull) UIImage *actionImage;

/**
 @internal property returns default compression that should be used for an image
 */
@property (nonatomic, readonly, nonnull) NSNumber *actionImageCompr;

@end
