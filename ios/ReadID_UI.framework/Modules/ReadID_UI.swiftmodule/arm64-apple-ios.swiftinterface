// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.1.3 (swiftlang-1100.0.282.1 clang-1100.0.33.15)
// swift-module-flags: -target arm64-apple-ios11.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name ReadID_UI
import AVFoundation
import AudioToolbox
import Combine
import CoreFoundation
import CoreGraphics
import CoreNFC
import Foundation
import ReadID
import Swift
import SwiftUI
import UIKit
import os
@objc @available(iOS 13, *)
public class InternalReadIDButton : UIKit.UIButton {
  @objc @IBInspectable public var cornerRadius: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var shadowOpacity: Swift.Float {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var borderWidth: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var normalBorderColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var pressedBorderColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var selectedBorderColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var disabledBorderColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var normalTintColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var pressedTintColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var selectedTintColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var disabledTintColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var normalBackgroundColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var pressedBackgroundColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var selectedBackgroundColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var disabledBackgroundColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc override dynamic public var isHighlighted: Swift.Bool {
    @objc get
    @objc set
  }
  @objc override dynamic public var isSelected: Swift.Bool {
    @objc get
    @objc set
  }
  @objc override dynamic public var isEnabled: Swift.Bool {
    @objc get
    @objc set
  }
  @objc deinit
}
@available(iOS 13, *)
public struct MRZResult {
  public let documentType: ReadID_UI.DocumentType
  public let mrzData: ReadID.MRZData?
  public let mrzImage: UIKit.UIImage?
  public let vizImage: UIKit.UIImage?
  public let totalScanTime: Foundation.TimeInterval?
  public let nettoScanTime: Foundation.TimeInterval?
  public var nfcAccessKey: ReadID_UI.NFCAccessKey?
}
@available(iOS 13, *)
public enum ReadIDError : Swift.Error {
  case invalidConfiguration(msg: Swift.String)
  case internalNFCNotSupported
}
@available(iOS 13, *)
public enum DateFormat : Swift.String {
  case DDMMYY
  case MMDDYY
  case YYMMDD
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
@available(iOS 13, *)
public struct NFCResult {
  public let readIDSession: ReadID.ReadIDSession
  public let faceImage: UIKit.UIImage?
  public let signatureImage: UIKit.UIImage?
}
@available(iOS 13, *)
public class ReadIDUI {
  public static let resourcesConfiguration: ReadID_UI.ResourcesConfiguration
  public enum PresentationStyle {
    case modal(presentationStyle: UIKit.UIModalPresentationStyle)
    case push
  }
  @objc deinit
}
@available(iOS 13, *)
public class ResourcesConfiguration : Swift.CustomDebugStringConvertible {
  public var customBundle: Foundation.Bundle?
  public var headerImage: UIKit.UIImage? {
    get
    set
  }
  public var backgroundColor: UIKit.UIColor? {
    get
    set
  }
  public var textColor: UIKit.UIColor? {
    get
    set
  }
  public var sectionHeaderColor: UIKit.UIColor? {
    get
    set
  }
  public var sectionHeaderBackgroundColor: UIKit.UIColor? {
    get
    set
  }
  public var progressFilledColor: UIKit.UIColor? {
    get
    set
  }
  public var progressEmptyColor: UIKit.UIColor? {
    get
    set
  }
  public var primaryButtonColor: UIKit.UIColor? {
    get
    set
  }
  public var primaryButtonPressedColor: UIKit.UIColor? {
    get
    set
  }
  public var primaryButtonDisabledColor: UIKit.UIColor? {
    get
    set
  }
  public var primaryButtonTextColor: UIKit.UIColor? {
    get
    set
  }
  public var primaryButtonPressedTextColor: UIKit.UIColor? {
    get
    set
  }
  public var primaryButtonDisabledTextColor: UIKit.UIColor? {
    get
    set
  }
  public var primaryButtonBorderColor: UIKit.UIColor? {
    get
    set
  }
  public var primaryButtonPressedBorderColor: UIKit.UIColor? {
    get
    set
  }
  public var primaryButtonDisabledBorderColor: UIKit.UIColor? {
    get
    set
  }
  public var mrzWireframeColor: UIKit.UIColor? {
    get
    set
  }
  public var mrzWireframeOverlayOutsideColor: UIKit.UIColor? {
    get
    set
  }
  public var mrzWireframeOverlayInsideColor: UIKit.UIColor? {
    get
    set
  }
  public var mrzWireframeOverlayScanAreaColor: UIKit.UIColor? {
    get
    set
  }
  public var iconButtonColor: UIKit.UIColor? {
    get
    set
  }
  public var iconButtonPressedColor: UIKit.UIColor? {
    get
    set
  }
  public var iconButtonDisabledColor: UIKit.UIColor? {
    get
    set
  }
  public var tabTextColor: UIKit.UIColor? {
    get
    set
  }
  public var tabSelectedTextColor: UIKit.UIColor? {
    get
    set
  }
  public var tabIndicatorColor: UIKit.UIColor? {
    get
    set
  }
  public var errorColor: UIKit.UIColor? {
    get
    set
  }
  public var warningColor: UIKit.UIColor? {
    get
    set
  }
  public var isDynamicTypeEnabled: Swift.Bool
  public var textSize: CoreGraphics.CGFloat
  public var sectionHeaderTextSize: CoreGraphics.CGFloat
  public var textFont: UIKit.UIFont?
  public var buttonFont: UIKit.UIFont?
  public var textFieldFont: UIKit.UIFont?
  public var buttonHeight: CoreGraphics.CGFloat
  public var buttonCornerRadius: CoreGraphics.CGFloat
  public var buttonTextSize: CoreGraphics.CGFloat
  public var buttonTextWeight: UIKit.UIFont.Weight
  public var buttonStrokeWidth: CoreGraphics.CGFloat
  public var buttonContentInsets: UIKit.UIEdgeInsets
  public var buttonIconSize: CoreGraphics.CGFloat
  public var wireframePadding: CoreGraphics.CGFloat
  public var wireframeStrokeWidth: CoreGraphics.CGFloat
  public func styleButton(_ button: ReadID_UI.InternalReadIDButton, styleBackground: Swift.Bool = true, setEdgeInsets: Swift.Bool = true)
  public func styleLabel(_ label: UIKit.UILabel, style: UIKit.UIFont.TextStyle = .body, font: UIKit.UIFont? = nil, textSize: CoreGraphics.CGFloat? = nil)
  public func styleTextField(_ textField: UIKit.UITextField, style: UIKit.UIFont.TextStyle = .body, font: UIKit.UIFont? = nil)
  public func styleSegmentedControl(_ segmentedControl: UIKit.UISegmentedControl)
  public func styleProgressView(_ progressView: UIKit.UIProgressView)
  @objc deinit
}
@available(iOS 13, *)
public enum MRZResultMode : Swift.String, Swift.CaseIterable {
  case none
  case accessControl
  case advancePassengerInformation
  case all
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
  public typealias AllCases = [ReadID_UI.MRZResultMode]
  public static var allCases: [ReadID_UI.MRZResultMode] {
    get
  }
}
@available(iOS 13, *)
public struct MRZConfiguration : Swift.CustomDebugStringConvertible {
  public var documentTypeButtons: [ReadID_UI.DocumentType]?
  public var allowedDocumentTypes: [ReadID_UI.DocumentType]?
  public var mrzResultMode: ReadID_UI.MRZResultMode
  public var isShowMRZFieldImagesEnabled: Swift.Bool
  public var isShowMRZTextEnabled: Swift.Bool
  public var isShowMRZImageEnabled: Swift.Bool
  public var isShowVIZImageEnabled: Swift.Bool
  public var isShowOCRScanTimeEnabled: Swift.Bool
  public var isManualInputEnabled: Swift.Bool
  public var isCheckMRZFieldScoresEnabled: Swift.Bool
  public var isCheckMRZCheckDigitsEnabled: Swift.Bool
  public var isCheckMRZAssumptionsEnabled: Swift.Bool
  public var diligence: ReadID_UI.Diligence
  public init()
}
@available(iOS 13, *)
public enum ReadIDResult {
  case ok
  case failed(reason: ReadID_UI.FailedReason)
  case closed
  case backNavigation
}
@available(iOS 13, *)
public enum FailedReason : Swift.Error {
  case backgroundTimeout
  case sessionTimeout
  case sessionError
  case connectionSecurityError
  case invalidAccessControl
  case connectionProblem
  case nfcError
  case userCancelled
  case cameraPermissionDenied
  public static func == (a: ReadID_UI.FailedReason, b: ReadID_UI.FailedReason) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
}
@available(iOS 13, *)
public enum NFCResultMode : Swift.String, Swift.CaseIterable {
  case none
  case simple
  case all
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
  public typealias AllCases = [ReadID_UI.NFCResultMode]
  public static var allCases: [ReadID_UI.NFCResultMode] {
    get
  }
}
@available(iOS 13, *)
extension Configuration {
  public var isCommitMRZOnlySessionEnabled: Swift.Bool {
    get
    set
  }
}
@available(iOS 13, *)
public protocol ReadIDEventDataParameter {
  var name: Swift.String { get }
  var value: Swift.String { get }
}
@available(iOS 13, *)
public enum ReadIDEventResult : Swift.String, ReadID_UI.ReadIDEventDataParameter {
  case succeeded, canceled, paused, failed
  public var name: Swift.String {
    get
  }
  public var value: Swift.String {
    get
  }
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
@available(iOS 13, *)
public enum ReadIDEventDocumentType : Swift.String, ReadID_UI.ReadIDEventDataParameter {
  case passport, idCard, visa, driversLicense, vehicleRegistration
  public var name: Swift.String {
    get
  }
  public var value: Swift.String {
    get
  }
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
@available(iOS 13, *)
public enum ReadIDEventAnimationFinishedMethod : Swift.String, ReadID_UI.ReadIDEventDataParameter {
  case button, auto
  public var name: Swift.String {
    get
  }
  public var value: Swift.String {
    get
  }
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
@available(iOS 13, *)
public enum ReadIDEventNFCVerificationMethod : Swift.String, ReadID_UI.ReadIDEventDataParameter {
  case local, remote
  public var name: Swift.String {
    get
  }
  public var value: Swift.String {
    get
  }
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
@available(iOS 13, *)
public enum ReadIDEventNFCReadingFailureReason : Swift.String, ReadID_UI.ReadIDEventDataParameter {
  case accessControl, tagLost
  public var name: Swift.String {
    get
  }
  public var value: Swift.String {
    get
  }
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
@available(iOS 13, *)
public struct NFCConfiguration : Swift.CustomDebugStringConvertible {
  public init()
  public var nfcAccessKey: ReadID_UI.NFCAccessKey?
  public var documentTypeButtons: [ReadID_UI.DocumentType]?
  public var nfcResultMode: ReadID_UI.NFCResultMode
  public var isReadImagesEnabled: Swift.Bool
  public var isShowFaceImageEnabled: Swift.Bool
  public var isShowVerificationResultEnabled: Swift.Bool
  public var isShowSecurityResultEnabled: Swift.Bool
  public var skipButtonAttempts: Swift.Int
}
@available(iOS 13, *)
public enum Diligence : Swift.String {
  case low
  case medium
  case high
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
@available(iOS 13, *)
extension CustomDebugStringConvertible {
  public var debugDescription: Swift.String {
    get
  }
}
@available(iOS 13, *)
extension ReadIDUI {
  public typealias CompletionHandler = (ReadID_UI.ReadIDResult, ReadID_UI.MRZResult?, ReadID_UI.NFCResult?) -> ()
  public typealias IntermediateUIHandler = (ReadID_UI.MRZResult, @escaping () -> ()) -> (Swift.Bool)
  public static func identify(from viewController: UIKit.UIViewController, style: ReadID_UI.ReadIDUI.PresentationStyle = .modal(presentationStyle: .fullScreen), session: ReadID.ReadIDSession? = nil, configuration: ReadID_UI.Configuration, mrzConfiguration: ReadID_UI.MRZConfiguration?, nfcConfiguration: ReadID_UI.NFCConfiguration?, intermediateUI: ReadID_UI.ReadIDUI.IntermediateUIHandler? = nil, tracker: ReadID_UI.ReadIDTracker? = nil, handler: @escaping ReadID_UI.ReadIDUI.CompletionHandler) throws
  public static func createViewController(session: ReadID.ReadIDSession? = nil, configuration: ReadID_UI.Configuration, mrzConfiguration: ReadID_UI.MRZConfiguration?, nfcConfiguration: ReadID_UI.NFCConfiguration?, intermediateUI: ReadID_UI.ReadIDUI.IntermediateUIHandler? = nil, tracker: ReadID_UI.ReadIDTracker? = nil, handler: @escaping ReadID_UI.ReadIDUI.CompletionHandler) throws -> UIKit.UIViewController
  public static var isInternalNFCSupported: Swift.Bool {
    get
  }
}
@available(iOS 13, *)
public enum DateSeparator : Swift.String {
  case hyphens
  case strokes
  case dots
  case spaces
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
  public init?(rawValue: Swift.String)
}
@available(iOS 13, *)
public enum ReadIDEventData {
  case mrzProcessStarted
  case mrzProcessFinished
  case nfcProcessStarted
  case nfcProcessFinished
  case documentSelectionStarted
  case documentSelectionFinished(ReadID_UI.ReadIDEventResult)
  case documentSelectionClicked(ReadID_UI.ReadIDEventDocumentType)
  case mrzAnimationStarted
  case mrzAnimationFinished(ReadID_UI.ReadIDEventResult, _: ReadID_UI.ReadIDEventAnimationFinishedMethod? = nil)
  case mrzStarted
  case mrzFinished(ReadID_UI.ReadIDEventResult, _: ReadID_UI.ReadIDEventDocumentType? = nil)
  case mrzCameraPermissionGranted
  case mrzCameraPermissionDeclined
  case mrzHelpClicked
  case mrzManualInputClicked
  case mrzTorchClicked
  case mrzManualInputStarted
  case mrzManualInputFinished(ReadID_UI.ReadIDEventResult)
  case mrzHelpStarted
  case mrzHelpFinished(ReadID_UI.ReadIDEventResult)
  case mrzResultStarted
  case mrzResultFinished(ReadID_UI.ReadIDEventResult)
  case nfcAnimationStarted
  case nfcAnimationFinished(ReadID_UI.ReadIDEventResult, _: ReadID_UI.ReadIDEventAnimationFinishedMethod? = nil)
  case nfcHelpStarted
  case nfcHelpFinished(ReadID_UI.ReadIDEventResult)
  case nfcStarted
  case nfcFinished(ReadID_UI.ReadIDEventResult)
  case nfcSkipClicked
  case nfcCancelClicked
  case nfcTagFound
  case nfcReadProcessStarted
  case nfcVerificationStarted(ReadID_UI.ReadIDEventNFCVerificationMethod)
  case nfcVerificationFinished(ReadID_UI.ReadIDEventNFCVerificationMethod)
  case nfcReadProcessFinished(ReadID_UI.ReadIDEventResult, _: ReadID_UI.ReadIDEventNFCReadingFailureReason? = nil)
  case nfcResultStarted
  case nfcResultFinished(ReadID_UI.ReadIDEventResult)
  public var parameters: [Swift.String : Swift.String] {
    get
  }
  public var description: Swift.String {
    get
  }
  public var name: Swift.String {
    get
  }
}
@available(iOS 13, *)
public protocol ReadIDTracker {
  func trackEvent(_ event: ReadID_UI.ReadIDEvent)
}
@available(iOS 13, *)
public enum DocumentType : Swift.String {
  case passport
  case idCard
  case visa
  case driversLicense
  case vehicleRegistration
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
@available(iOS 13, *)
public struct Configuration : Swift.CustomDebugStringConvertible {
  public var baseUrl: Swift.String {
    get
    set
  }
  public var accessKey: Swift.String?
  public var oauthToken: Swift.String?
  public var opaqueId: Swift.String?
  public var isPinningEnabled: Swift.Bool
  public var isShowInstructionsEnabled: Swift.Bool
  public var instructionsReplays: Swift.Int
  public var isShowInstructionsAlwaysEnabled: Swift.Bool
  public var isShowInstructionButtonEnabled: Swift.Bool
  public var isMaskPersonalDataEnabled: Swift.Bool
  public var dateFormat: ReadID_UI.DateFormat
  public var dateSeparator: ReadID_UI.DateSeparator
  public var backgroundTimeout: Foundation.TimeInterval
  public var isAskDismissConfirmEnabled: Swift.Bool
  public var analyticsEnabled: Swift.Bool
  public var dataminingURL: Foundation.URL?
  public var isCommitSessionForUserSkippedEnabled: Swift.Bool
  public var isCommitMRZOnlySessionForNoChipEnabled: Swift.Bool
  public init()
}
@available(iOS 13, *)
public enum NFCAccessKey {
  case icao(documentType: ReadID_UI.DocumentType, documentNumber: Swift.String, dateOfBirth: Swift.String, dateOfExpiry: Swift.String)
  case edl(mrz: Swift.String)
}
@available(iOS 13, *)
extension ReadIDSession {
}
@available(iOS 13, *)
public struct ReadIDEvent : Swift.CustomStringConvertible {
  public let data: ReadID_UI.ReadIDEventData
  public let timestamp: Swift.Double
  public var description: Swift.String {
    get
  }
}
@available(iOS 13, *)
extension MRZResult {
  public var readIDSession: ReadID.ReadIDSession? {
    get
  }
}
@available(iOS 13, *)
extension ReadID_UI.DateFormat : Swift.Equatable {}
@available(iOS 13, *)
extension ReadID_UI.DateFormat : Swift.Hashable {}
@available(iOS 13, *)
extension ReadID_UI.DateFormat : Swift.RawRepresentable {}
@available(iOS 13, *)
extension ReadID_UI.DocumentType : Swift.Hashable {}
@available(iOS 13, *)
extension ReadID_UI.DocumentType : Swift.RawRepresentable {}
@available(iOS 13, *)
extension ReadID_UI.MRZResultMode : Swift.Equatable {}
@available(iOS 13, *)
extension ReadID_UI.MRZResultMode : Swift.Hashable {}
@available(iOS 13, *)
extension ReadID_UI.MRZResultMode : Swift.RawRepresentable {}
@available(iOS 13, *)
extension ReadID_UI.FailedReason : Swift.Equatable {}
@available(iOS 13, *)
extension ReadID_UI.FailedReason : Swift.Hashable {}
@available(iOS 13, *)
extension ReadID_UI.NFCResultMode : Swift.Hashable {}
@available(iOS 13, *)
extension ReadID_UI.NFCResultMode : Swift.RawRepresentable {}
@available(iOS 13, *)
extension ReadID_UI.ReadIDEventResult : Swift.Equatable {}
@available(iOS 13, *)
extension ReadID_UI.ReadIDEventResult : Swift.Hashable {}
@available(iOS 13, *)
extension ReadID_UI.ReadIDEventResult : Swift.RawRepresentable {}
@available(iOS 13, *)
extension ReadID_UI.ReadIDEventDocumentType : Swift.Equatable {}
@available(iOS 13, *)
extension ReadID_UI.ReadIDEventDocumentType : Swift.Hashable {}
@available(iOS 13, *)
extension ReadID_UI.ReadIDEventDocumentType : Swift.RawRepresentable {}
@available(iOS 13, *)
extension ReadID_UI.ReadIDEventAnimationFinishedMethod : Swift.Equatable {}
@available(iOS 13, *)
extension ReadID_UI.ReadIDEventAnimationFinishedMethod : Swift.Hashable {}
@available(iOS 13, *)
extension ReadID_UI.ReadIDEventAnimationFinishedMethod : Swift.RawRepresentable {}
@available(iOS 13, *)
extension ReadID_UI.ReadIDEventNFCVerificationMethod : Swift.Equatable {}
@available(iOS 13, *)
extension ReadID_UI.ReadIDEventNFCVerificationMethod : Swift.Hashable {}
@available(iOS 13, *)
extension ReadID_UI.ReadIDEventNFCVerificationMethod : Swift.RawRepresentable {}
@available(iOS 13, *)
extension ReadID_UI.ReadIDEventNFCReadingFailureReason : Swift.Equatable {}
@available(iOS 13, *)
extension ReadID_UI.ReadIDEventNFCReadingFailureReason : Swift.Hashable {}
@available(iOS 13, *)
extension ReadID_UI.ReadIDEventNFCReadingFailureReason : Swift.RawRepresentable {}
@available(iOS 13, *)
extension ReadID_UI.Diligence : Swift.Equatable {}
@available(iOS 13, *)
extension ReadID_UI.Diligence : Swift.Hashable {}
@available(iOS 13, *)
extension ReadID_UI.Diligence : Swift.RawRepresentable {}
@available(iOS 13, *)
extension ReadID_UI.DateSeparator : Swift.Equatable {}
@available(iOS 13, *)
extension ReadID_UI.DateSeparator : Swift.Hashable {}
@available(iOS 13, *)
extension ReadID_UI.DateSeparator : Swift.RawRepresentable {}
