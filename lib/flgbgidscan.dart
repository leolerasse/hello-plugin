import 'dart:async';

import 'package:flutter/services.dart';

class Flgbgidscan {
  static const MethodChannel _channel =
      const MethodChannel('flgbgidscan');

  static const Map<String, String> _credentials = {
    "baseUrl": "https://poc.idscan.cloud",
    "username": "VMSuper",
    "password": "!Mon01+"
  };

  static Future<String> get startUserJourney async {
    final String res = await _channel.invokeMethod('startUserJourney', _credentials);
    return res;
  }
}
