import 'package:flutter/material.dart';

import 'package:flutter/services.dart';
import 'package:flgbgidscan/flgbgidscan.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _userJourneyRes = "0000";
  @override
  void initState() {
    super.initState();
  }

  _startUserJourney() async {
    String userJourneyRes;

    try {
      userJourneyRes = await Flgbgidscan.startUserJourney;
    } on PlatformException {
      userJourneyRes = 'Failed to get platform version.';
    }

    setState(() {
      _userJourneyRes = userJourneyRes;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Column(children: <Widget>[
          RaisedButton(
            onPressed: _startUserJourney,
            child: Text("Start UserJourney"),
          ),
          Text("User Journey: $_userJourneyRes")
        ]),
      ),
    );
  }
}
