//
//  IDSEnterpriseSession.h
//  IDSWebServices
//
//  Created by Edvardas Maslauskas on 13/03/2017.
//  Copyright © 2017 IDscan Biometrics Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IDSEnterpriseCredentials;

typedef NS_ENUM(NSInteger, IDSEnterpriseInternalAuthenticationResult) {
    IDSEnterpriseInternalAuthenticationResultSuccessful = 0,
    IDSEnterpriseInternalAuthenticationResultInvalidCredentials,
    IDSEnterpriseInternalAuthenticationResultError
};

@interface IDSEnterpriseSession : NSObject

+ (void)authenticateEnterpriseSessionWithCredentials:(IDSEnterpriseCredentials *)credentials async:(BOOL)isAsync
                                      WithCompletion:(void (^_Nonnull)(IDSEnterpriseInternalAuthenticationResult result, NSError *_Nullable error, NSString *_Nullable token))completionHandler;

+ (void)postRequestWithPath:(NSString * _Nonnull)path
                     params:(NSDictionary *_Nullable)params
                credentials:(IDSEnterpriseCredentials * _Nonnull)credentials
                   progress:(void (^_Nullable)(NSProgress * _Nonnull uploadProgress))progressHandler
          completionHandler:(void (^_Nonnull)(id _Nullable responseObject, NSError *_Nullable error))completionHandler;

+ (void)getRequestWithPath:(NSString * _Nonnull)path
                    params:(NSDictionary *_Nullable)params
               credentials:(IDSEnterpriseCredentials * _Nonnull)credentials
                  progress:(void (^_Nullable)(NSProgress * _Nonnull uploadProgress))progressHandler
         completionHandler:(void (^_Nonnull)(id _Nullable responseObject, NSError *_Nullable error))completionHandler;

+ (void)uploadTaskWithRequest:(NSMutableURLRequest * _Nonnull)request
                  credentials:(IDSEnterpriseCredentials *_Nonnull)credentials
                     progress:(void (^_Nullable)(NSProgress *_Nonnull uploadProgress))progressHandler
            completionHandler:(void (^_Nonnull)(id _Nullable parameters, NSError *_Nullable error))completionHandler;
@end
