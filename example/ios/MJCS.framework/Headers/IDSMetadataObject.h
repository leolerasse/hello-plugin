//
//  IDSMetadataObject.h
//  MJCS SDK
//
//  Created by Stanislav Kozyrev <s.kozyrev@idscan.co.uk>.
//  Copyright (c) 2014 IDScan Biometrics Ltd. All rights reserved.
//

#import "IDSBase.h"

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@class IDSMetadataDocumentObjectFields;

/**
 * @brief An IDSMetadataObjectTypeMachineReadableZone instance represents a
 * single detected machine readable zone in an image.
 */
MJCS_EXPORT NSString *const _Nonnull IDSMetadataObjectTypeMachineReadableZone;

/**
 * @brief An IDSMetadataObjectTypeDocumentClassification instance represents a single
 * detected document classification.
 */
MJCS_EXPORT NSString *const _Nonnull IDSMetadataObjectTypeDocumentClassification;

/**
 * @brief The IDSMetadataObject class defines the basic properties associated
 * with a piece of metadata. These attributes reflect information either about
 * the metadata itself or the media from which the metadata originated.
 * Subclasses are responsible for providing appropriate values for each of the
 * relevant properties.
 */
MJCS_CLASS_EXPORT
@interface IDSMetadataObject : NSObject

/**
 * @brief The error corrected data decoded into a human-readable string or a nil
 * value if a string representation cannot be created.
 */
@property (readonly, nonatomic, nullable) NSString *stringValue;

/**
 * @brief The type of the metadata.
 */
@property (readonly, nonatomic, nullable) NSString *type;

@end

/**
 * @brief IDSMetadataMachineReadableZoneElementObject defines the chunk of
 * information read from machine readable zone. Defines its value, confidence
 * and location on the processed image.
 */
MJCS_CLASS_EXPORT
@interface IDSMetadataMachineReadableZoneElementObject : NSObject

/**
 * @brief value of mrz element.
 */
@property (nonatomic, nonnull) NSString *value;

/**
 * @brief confidence of mrz element value
 * @deprecated Property was deprecated and will not return any result
 */
@property (nonatomic) NSInteger confidence DEPRECATED_MSG_ATTRIBUTE("Property was deprecated and will not return any result");

/**
 * @brief top left corner of mrz element location
 */
@property (nonatomic) CGPoint topLeft;

/**
 * @brief top right corner of mrz element location
 */
@property (nonatomic) CGPoint topRight;

/**
 * @brief bottom left corner of mrz element location
 */
@property (nonatomic) CGPoint bottomLeft;

/**
 * @brief bottom right corner of mrz element location
 */
@property (nonatomic) CGPoint bottomRight;

@end

/**
 * @brief An IDSMetadataMachineReadableZoneObject instance defines a recognized
 * machine readable zone object in an image. It is an immutable object
 * describing some extracted document features.
 */
MJCS_CLASS_EXPORT
@interface IDSMetadataMachineReadableZoneObject : IDSMetadataObject

/**
 * @brief A mandatory value that designates the holder's date of birth.
 */
@property (readonly, nonatomic, nullable) IDSMetadataMachineReadableZoneElementObject *dateOfBirth;

/**
 * @brief A mandatory value that designates the document's date of expiry or
 * valid until date.
 */
@property (readonly, nonatomic, nullable) IDSMetadataMachineReadableZoneElementObject *dateOfExpiryOrValidUntil;

/**
 * @brief A mandatory value that designates the document number.
 */
@property (readonly, nonatomic, nullable) IDSMetadataMachineReadableZoneElementObject *documentNumber;

/**
 * @brief A mandatory value that designates the particular type of the document.
 */
@property (readonly, nonatomic, nullable) IDSMetadataMachineReadableZoneElementObject *documentPrimaryType;

/**
 * @brief An optional value that is used to further identify the document at
 * the discretion of the issuing state or organization.
 */
@property (readonly, nonatomic, nullable) IDSMetadataMachineReadableZoneElementObject *documentSecondaryType;

/**
 * @brief A mandatory value that designates three-letter code (Alpha-3) used to
 * indicate the issuing state or organization.
 */
@property (readonly, nonatomic, nullable) IDSMetadataMachineReadableZoneElementObject *issuingStateOrOrganization;

/**
 * @brief A mandatory value that designates the first name of the holder.
 */
@property (readonly, nonatomic, nullable) IDSMetadataMachineReadableZoneElementObject *firstName;

/**
 * @brief A mandatory value that designates the middle name of the holder.
 */
@property (readonly, nonatomic, nullable) IDSMetadataMachineReadableZoneElementObject *middleName;

/**
 * @brief A mandatory value that designates the last name of the holder.
 */
@property (readonly, nonatomic, nullable) IDSMetadataMachineReadableZoneElementObject *lastName;

/**
 * @brief A mandatory value that designates three-letter code (Alpha-3) used to
 * indicate the nationality of the holder.
 */
@property (readonly, nonatomic, nullable) IDSMetadataMachineReadableZoneElementObject *nationalityCode;

/**
 * @brief A mandatory value that designates the sex of the holder.
 */
@property (readonly, nonatomic, nullable) IDSMetadataMachineReadableZoneElementObject *sex;

/**
 * @brief A mandatory value that designates the full text of machine readable zone.
 */
@property (readonly, nonatomic, nullable) IDSMetadataMachineReadableZoneElementObject *fullMRZ;

/**
 * @brief A mandatory value that designates the first line of machine readable
 * zone on the document.
 */
@property (readonly, nonatomic, nullable) IDSMetadataMachineReadableZoneElementObject *mrzLine1;

/**
 * @brief A mandatory value that designates the second line of machine readable
 * zone on the document.
 */
@property (readonly, nonatomic, nullable) IDSMetadataMachineReadableZoneElementObject *mrzLine2;

/**
 * @brief A optional value that designates the third line of machine readable
 * zone on the document.
 */
@property (readonly, nonatomic, nullable) IDSMetadataMachineReadableZoneElementObject *mrzLine3;

/**
 * @brief A mandatory value that designates the type of ICAO (International Civil
 * Aviation Organization) document.
 */
@property (readonly, nonatomic, nullable) NSString *mrzType;

/**
 * @brief A mandatory value that designates the rotation angle of the analyzed
 * document.
 * @deprecated Property was deprecated and will return null
 */
@property (readonly, nonatomic) CGFloat deskewAngle DEPRECATED_MSG_ATTRIBUTE("Property was deprecated and will return null");

/**
 * @brief A value that is used to confirm that the machine readable zone
 * values were extracted correctly.
 * @deprecated Property was deprecated and will return null
 */
@property (readonly, nonatomic) BOOL valid DEPRECATED_MSG_ATTRIBUTE("Property was deprecated and will return null");

/**
 * @brief Cropped document image.
 */
@property (readonly, assign, nonatomic, nonnull) CGImageRef documentImage;

@end

MJCS_CLASS_EXPORT

/**
 * @brief IDSMetadataClassificationObject defines the chunk of
 * information read from classified document.
 */
@interface IDSMetadataClassificationObject : IDSMetadataObject

/**
 * @brief Three letter abbreviation of document type.
 */
@property (nonatomic, readonly, nullable) NSString *documentType;
/**
 * @brief Name of the country from which the document friendly name.
 */
@property (nonatomic, readonly, nullable) NSString *friendlyName;
/**
 * @brief The document rotration degree.
 * @deprecated Property was deprecated and will return null
 */
@property (nonatomic, readonly, nullable) NSNumber *rotationDegree DEPRECATED_MSG_ATTRIBUTE("Property was deprecated and will return null");
/**
 * @brief Classification score
 * @deprecated Property was deprecated and will return null
 */
@property (nonatomic, readonly, nullable) NSNumber *score DEPRECATED_MSG_ATTRIBUTE("Property was deprecated and will return null");
/**
 * @brief the confidence of document classification
 * @deprecated Property was deprecated and will return null
 */
@property (nonatomic, readonly, nullable) NSNumber *confidence DEPRECATED_MSG_ATTRIBUTE("Property was deprecated and will return null");
/**
 * @brief CGImageRef document image.
 */
@property (nonatomic, readonly, nonnull) CGImageRef documentImage;

@end
