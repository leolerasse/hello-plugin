//
//  IDSLivenessOverlayProtocol.h
//  MJCS
//
//  Created by Edvardas Maslauskas on 05/06/2018.
//  Copyright © 2018 IDScan Biometrics. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IDSLivenessActionState;

__attribute__((deprecated("IDSLivenessOverlayProtocol is deprecated since 9.0.0 version. Please use protocols adhering to IDSLivenessOfflineViewController or IDSLivenessOnlineViewController")))
/**
 Protocol used for customer overlay controller for liveness detection
 */
@protocol IDSLivenessOverlayProtocol <NSObject>

/**
 Delegate method to notify that liveness requested certain action to be performed
 and displayed on the screen for a customer
 */
@required
- (void)requestedAction:(nonnull IDSLivenessActionState *)action;

/**
 Delegate method to notify that liveness needs to calibrate initial device position
 
 @discussion in order to capture best frames possible and not to loose customer's face coordinates
 we should calibrate initial device position as straight as possible. Ideally we want it to be maximum 8.0 degrees of offset.
 
 The logic should be the following:
 
 facePose.y > 8.0 -> Move device down
 facePose.y < -8.0 -> Move device up
 facePose.x > 8.0 -> Move device to the left
 facePose.x < -8.0 -> Move device to the right

  @param facePose coordinates of face pose compare to the device angle.
 */
@required
- (void)requestedCalibration:(CGPoint)facePose;

/**
 Delegate method, which will be called once liveness is sucessfuly loaded
 @discussion ideally we should be displaying some pre-loader while we load some liveness as on very old devices
 it could take a bit of time
 */
@optional
- (void)livenessLoadingFinished;

@end
