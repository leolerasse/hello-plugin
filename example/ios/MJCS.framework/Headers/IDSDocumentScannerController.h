//
//  IDSDocumentScannerController.h
//  MJCS SDK
//
//  Created by Stanislav Kozyrev <s.kozyrev@idscan.co.uk>.
//  Modified by Tomas Zemaitis <t.zemaitis@idscan.co.uk>.
//  Modified by Edvardas Maslauskas <e.maslauskas@idscan.com>
//  Copyright (c) 2014 IDScan Biometrics Ltd. All rights reserved.
//

#import "IDSBase.h"
#import <UIKit/UIKit.h>

@class IDSDocumentScannerController;
@class IDSDocumentScannerConfig;

#pragma mark - Scanner delegate protocol

/**
 * The metadata object containing document scanning result. The
 * value is of type IDSMetadataDocumentObject if scanner type is
 * IDSDocumentScannerTypeDocument
 */
MJCS_EXPORT NSString * _Nonnull const IDSDocumentScannerInfoMetadataObject;

/**
 * @brief The extracted image on which scanner procedure was performed. The
 * Value is of type UIImage.
 */
MJCS_EXPORT NSString * _Nonnull const IDSDocumentScannerInfoImage;


/**
 * The IDSDocumentCaptureControllerDelegate protocol defines methods
 * that your delegate object must implement to interact with the document
 * capture interface.
 */
MJCS_CLASS_EXPORT
@protocol IDSDocumentScannerControllerDelegate <NSObject>

@optional
/**
 * Tells the delegate that the scanner is unable to perform its duties.
 */
- (void)documentScannerController:(IDSDocumentScannerController *_Nonnull)scanner didFailWithError:(NSError *_Nullable)error;

@optional
/**
 * Tells the delegate that the user cancelled the scan operation.
 */
- (void)documentScannerControllerDidCancel:(IDSDocumentScannerController *_Nonnull)scanner;

@optional
/**
 * Tells the delegate that the scanner recognized a document object
 * in the image.
 */
- (void)documentScannerController:(IDSDocumentScannerController *_Nonnull)scanner didFinishScanningWithInfo:(NSDictionary *_Nonnull)info;

@end

#pragma mark - Scanner class interface
/**
 *  The initial image that is passed to the document scanner controller,
 *  in case the user just wants to pass an image from the camera roll for cropping
 */
MJCS_EXPORT NSString * _Nonnull const IDSDocumentScannerOptionImage;

/**
 *  Disables 4K video processing, in case if application consumer big amount of memory on it's own
 *  hence document scanner can cause memory warnings as 4K capture requires substantial amount of it.
 *  Default value: enabled
 */
MJCS_EXPORT NSString * _Nonnull const IDSDocumentScannerOptionDisable4K;

/**
 * Disables manual capture button on the camera screen, the value passed as an option should be NSNumber with BOOL
 * And it's only valid when scanner type is Document
 */
MJCS_EXPORT NSString * _Nonnull const IDSDocumentScannerOptionManualCaptureDisabled;

/**
 *  This is where you indicate the type of scanner you want to instantiate.
 */
typedef NS_ENUM(NSInteger, IDSDocumentScannerControllerType) {
    /**
     *  For general document scanning and extraction
     */
    IDSDocumentScannerControllerTypeDocument = 0,
    
    /**
     *  For A4 document scanning and extraction
     */
    IDSDocumentScannerControllerTypeUtility = 1,
    
    /**
     *  For Selfie scanning
     */
    IDSDocumentScannerControllerTypeSelfie = 2
};

/**
 * The IDSDocumentScannerController class manages framework-supplied user
 * interfaces for scanning documents. A document scanner controller manages user
 * interactions and delivers the results of those interactions to a delegate
 * object.
 */
MJCS_CLASS_EXPORT
@interface IDSDocumentScannerController : UIViewController

/**
 * The document scanner’s delegate object.
 */
@property (weak, nonatomic, nullable) id<IDSDocumentScannerControllerDelegate> delegate;

/**
 *  Returns a new instance of the document scanner initialized according to the specified options
 *
 *  @param type    IDSDocumentScannerType enum to indicate what kind of scanner to use
 *  @param options NSDictionary of options to configure the scanner. Can be nil.
 */
- (instancetype _Nonnull)initWithScannerType:(IDSDocumentScannerControllerType)type options:(NSDictionary *_Nullable)options DEPRECATED_MSG_ATTRIBUTE("This constructor is deprecated since 9.0.0 version. Please use initWithScannerConfig:");

/// Main constructor to initialise document scanner
/// @param config of the document scanner
- (instancetype _Nonnull )initWithScannerConfig:(IDSDocumentScannerConfig * _Nonnull)config;

@end
