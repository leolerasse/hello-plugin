//
//  IDSCustomerJourneyUIController.h
//  MJCS
//
//  Created by Edvardas Maslauskas on 24/02/2020.
//  Copyright © 2020 GB Group Plc. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol IDSCustomerJourneyUIUploadingProtocol <NSObject>

/// Returns networking error while uploading data to the backend
/// Once ready to proceed, view controller should call:
///     - finish: to repeat the step again
///     - cancel: to cancel the journey completely
/// @param error error object, which contains localized description
- (void)onUploadingError:(NSError *_Nonnull)error;

/// Called once uploading task has succesfully finished. Once ready to proceed
/// ViewController should call finish
- (void)onUploadingFinished;
@end

@interface IDSCustomerJourneyUIController : UIViewController

/// Should be called once UI is ready to continue to the scanning process
- (void)finish;

/// Should be called if we want to cancel the journey completely
- (void)cancel;

@end

NS_ASSUME_NONNULL_END
