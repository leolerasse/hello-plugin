//
//  IDESAuthenticationCheck.h
//  idesv2
//
//  Created by David Okun on 06/07/2016.
//  Copyright © 2016 IDscan Biometrics Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Returns state of the authentication check. For detailed information about
 * check state check `[IDESAuthenticationCheck stateDescription]` value.
 * @return state of authentication check. Possible values undetermined,
 *         passed, failed or skipped.
 */
typedef NS_ENUM(NSInteger, IDESAuthenticationCheckState) {
    /**
     *  If state is undetermined.
     */
    IDESAuthenticationCheckStateUndetermined = 0,
    /**
     *  If authentication check passed.
     */
    IDESAuthenticationCheckStatePassed = 1,
    /**
     *  If authentication check failed
     */
    IDESAuthenticationCheckStateFailed = 2,
    /**
     *  If authentication check skipped
     */
    IDESAuthenticationCheckStateSkipped = 3
};

/**
 *  Object which explains a particular authentication check performed on a returned document
 */
@interface IDESAuthenticationCheck : NSObject

/**
 * Returns a value representing identifier of the authentication check.
 * This identifier should be used to distiguish the check and refer to it
 * programatically if needed.
 * @return authentication check's id.
 */
@property (nonatomic, readonly, nonnull) NSString *id;

/**
 * Returns a value specifying the title of the authentication check.
 * This value can be used for friendly representation of the check to the
 * user. For identification purposes not related with friendly
 * representation, id field should be used
 * @return title of the authentication check.
 */
@property (nonatomic, readonly, nonnull) NSString *title;

/**
 * Returns a value specifying description of the authentication check.
 * @return description of authentication check.
 */
@property (nonatomic, readonly, nonnull) NSString *checkDescription;

/**
 * Returns the authentication check's group. This is used to categorize many
 * check instances that perform the same type of check (e.g. Checks of many
 * Ultraviolet patterns or many MRZ check digits).
 * @return group of authentication check.
 */
@property (nonatomic, readonly, nonnull) NSString *group;

/**
 * Returns state of the authentication check. For detailed information about
 * check state check `[IDESAuthenticationCheck stateDescription]` value.
 * @return state of authentication check. Possible values undetermined,
 *         passed, failed or skipped.
 */
@property (nonatomic, readonly) IDESAuthenticationCheckState state;

/**
 * Returns description of the authentication check state.
 * @return description of authentication check state.
 */
@property (nonatomic, readonly, nullable) NSString *stateDescription;

@end
