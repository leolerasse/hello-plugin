//
//  IDSEnterpriseActionDetail.h
//  IDSWebServices
//
//  Created by Dicle Yilmaz on 5.06.2018.
//  Copyright © 2018 IDscan Biometrics Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface IDSEnterpriseActionDetail : NSObject

/**
 * Last result image for each action of liveness
 */
@property (nonatomic, nullable, strong) UIImage *image;

/**
 * Last result action for each action of liveness
 */
@property (nonatomic, nonnull, strong) NSNumber *action;

/**
 * Last result for each action of liveness
 */
@property (nonatomic, nonnull, strong) NSNumber *result;

/**
 * compressionRate for the image that we send the server
 * Default value is 0.6
 */
@property (nonatomic, assign) CGFloat compressionRate;

- (instancetype)initWithImage:(nonnull UIImage *)image action:(nonnull NSNumber *)action result:(nonnull NSNumber *)result;
@end
