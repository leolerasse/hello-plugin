//
//  IDSLivenessResultMapper.h
//  MJCS
//
//  Created by Edvardas Maslauskas on 13/06/2018.
//  Copyright © 2018 IDScan Biometrics. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IDSLivenessResult;
@class IDSEnterpriseRequestLiveness;
@class IDSEnterpriseResponse;
@class IDSLivenessConfig;

@interface IDSLivenessMapper : NSObject
+ (nonnull IDSEnterpriseRequestLiveness *)mapEnterpriseRequest:(nonnull IDSLivenessResult *)livenessResult;
+ (nonnull IDSLivenessConfig *)mapLivenessConfig:(nonnull IDSEnterpriseResponse *)response;
@end
