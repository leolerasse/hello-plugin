//
//  IDESProfile.h
//  idesv2
//
//  Created by Abdulrhman Babelli on 9/5/18.
//  Copyright © 2018 GB Group Plc All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IDESCountry.h"

/**
 A processing mode for IDES engine. Each mode is created for different purposes and will return different results

 - IDESProcessingModeSmartCapture: This mode is used to capture an image of the document. The engine will only classify the document, perform image quality check and return cropped image
 - IDESProcessingModeSmartScan: Same as SmartCapture mode, however in additionally the engine will extract the field values and perform authentication checks. This mode is designed to extract info from the document offline on-device. Due to additional pipeline steps, the scan might be slower.
 */
typedef NS_ENUM(NSUInteger, IDESProcessingMode) {
    IDESProcessingModeSmartCapture = 0,
    IDESProcessingModeSmartScan = 1,
    IDESProcessingModeSmartScanDeprecated = 2,
    IDESProcessingModeSmartCaptureDeprecated = 3,
};

@interface IDESProfile : NSObject

/**
 The tag name of the profile
 */
@property (nonatomic, readwrite, nonnull) NSString *tag;

/**
 The version of the profile
 */
@property (nonatomic, readonly, nonnull) NSString *resourceVersion;

/**
 The version of the generator tool
 */
@property (nonatomic, readonly, nonnull) NSString *generatorVersion;

/**
 Countries that are included in the profile
 */
@property (nonatomic, readonly, nonnull) NSArray<IDESCountry *> *countries;

/**
 Unique identifier of the profile
 */
@property (nonatomic, readonly, nonnull) NSString *ruid;

/**
 Generation date of the profile
 */
@property (nonatomic, readonly, nonnull) NSDate *date;

/**
 Path to the profile
 */
@property (nonatomic, readwrite, nonnull) NSString *path;

/**
 Dictionary of the internal settings for IDES engine
 */
@property (nonatomic, readonly, nonnull) NSDictionary *settings;

/**
 Sets the processing mode for the profile.

 @param profile the processing mode, which the engine should use while scanning a document
 */
- (void)setProcessingMode:(IDESProcessingMode)profile;

@end
