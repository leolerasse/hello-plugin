//
//  MJCS.h
//  MJCS SDK
//
//  Created by Edvardas Maslauskas <e.maslauskas@idscan.com>.
//  Copyright (c) 2017 IDScan Biometrics Ltd. All rights reserved.
//

#import "IDSDocumentScannerController.h"
#import "IDSCustomerJourneyController.h"
#import "IDSEnterpriseService.h"
#import "IDSWebServices.h"
#import "IDSMetadataObject.h"
#import "IDESDocument.h"
#import "IDSLivenessViewController.h"
#import "IDSLivenessOverlayProtocol.h"
#import "IDSLivenessMapper.h"
#import "IDESProfile.h"
#import "IDESProfileManager.h"
#import "IDESCountry.h"
#import "IDSCustomerJourneyUIController.h"

@class IDSGlobalConfig;
@protocol IDSCameraCaptureControllerDelegate;

@interface MJCS : NSObject

/**
 Loads the SDK modules and required resources synchronously. You need to call this method before using it
 */
+ (void)loadSDK;

/**
 Loads the SDK modules and required resources asynchronously. You need to call this method before using it

 @param completion a block which will be called once init is finished. It might return an exception if something goes wrong
 */
+ (void)loadWithCompletion :(void (^_Nonnull)(BOOL, NSException*_Nullable))completion;

/**
 Method used to swap document processing profile to the one, which is specified

 @param profile an object, which needs to be loaded for document processing engine
 @param completion a block, which will be called once loading is finished
 */
+ (void)swapWithProfile :(IDESProfile *_Nonnull)profile withCompletion:(void (^_Nonnull)(BOOL, NSException*_Nullable))completion;

/**
 Method used to swap document processing profile with the specified tag

 @param tag name of the profile
 @param completion a block, which will be called once loading is finished
 */
+ (void)swapWithTag :(NSString *_Nonnull)tag withCompletion:(void (^_Nonnull)(BOOL, NSException*_Nullable))completion;

/**
 Method, which unloads all resources of the SDK from the memory
 Should be used when SDK is no longer required by the application
 */
+ (void)disposeSDK;

+ (MJCS * _Nonnull)sharedInstance;

@property (nonatomic, nonnull, setter=setGlobalConfig:) IDSGlobalConfig *globalConfig;

@end
