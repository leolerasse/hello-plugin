//
//  IDSLiveness.h
//  IDSLiveness
//
//  Created by Edvardas Maslauskas on 23/04/2018.
//  Copyright © 2018 GB Group plc ('GBG'). All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for IDSLiveness.
FOUNDATION_EXPORT double IDSLivenessVersionNumber;

//! Project version string for IDSLiveness.
FOUNDATION_EXPORT const unsigned char IDSLivenessVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <IDSLiveness/PublicHeader.h>

#import <IDSLiveness/IDSLivenessDetection.h>
