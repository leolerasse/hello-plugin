/**
 * @file RecognizerApi.h
 *
 * @mainpage %Recognizer API documentation
 *
 * @section Introduction
 *
 * This documentation gives an overview how to use %Recognizer scanning library. %Recognizer library is supported
 * on several platforms: Mac OS (x86_64 only, MacOS X 10.14 and newer), Linux (x86_64, requires GLIBC 2.13 or newer), 64-bit Windows
 * (requires Visual C++ 2017 redistributable package ([download it here](https://go.microsoft.com/fwlink/?LinkId=746572))), Android (requires API level 16 or higher)
 * and iOS 8.0 or newer. This documentation covers the usage of
 * %Recognizer API to perform scanning of images and retrieving the scan results. For information about platform
 * specific integration, refer to platform specific README document.
 *
 * @section quickStart Quick start
 *
 * In order to perform recognition, you first need to create a concrete recognizer object. Each recognizer object is specific
 * for each object that needs to be scanned. After creating required recognizer objects, you need to create recognizer runner
 * object that will use given recognizer objects to perform recognition of images.
 *
 * A simple example that uses USDL recognizer to scan information from barcode of the US Driver's Licenses would look like this:
 *
 * -# Step 0: Insert your license key
 *      @code
 *          MBRecognizerErrorStatus errorStatus = recognizerAPIUnlockWithLicenseKey( "Add license key here" );
 *          if ( errorStatus != RECOGNIZER_ERROR_STATUS_SUCCESS )
 *          {
 *              // handle failure
 *          }
 *      @endcode
 *
 * -# Step 1: Create and configure USDL recognizer
 *      @code
 *          MBUsdlRecognizer * usdlRecognizer = NULL;
 *
 *          MBUsdlRecognizerSettings usdlSett;
 *          usdlSett.nullQuietZoneAllowed = MB_TRUE;
 *          usdlSett.shouldScanUncertain  = MB_TRUE;
 *
 *          MBRecognizerErrorStatus errorStatus = = usdlRecognizerCreate( &usdlRecognizer, &usdlSett );
 *          if ( errorStatus != RECOGNIZER_ERROR_STATUS_SUCCESS )
 *          {
 *              // handle failure
 *          }
 *      @endcode
 *
 * -# Step 2: Create and configure recognizer runner
 *      @code
 *          MBRecognizerRunnerSettings runnerSett;
 *          runnerSett.allowMultipleResults = MB_FALSE;
 *
 *          MBRecognizerPtr recognizers[] = { usdlRecognizer };
 *
 *          runnerSett.numOfRecognizers = 1;
 *          runnerSett.recognizers = recognizers;
 *
 *          errorStatus = recognizerRunnerCreate( &recognizerRunner, &runnerSett );
 *          if ( errorStatus != RECOGNIZER_ERROR_STATUS_SUCCESS )
 *          {
 *              // handle failure
 *          }
 *      @endcode
 *
 * -# Step 3a: Perform the scan on the image. Image first needs to be created from from memory. To create image from memory buffer use recognizerImageCreateFromRawImage.
 *    @code
 *      int image_width, image_height, image_stride;
 *      MBByte * image_buffer;
 *
 *      // populate above variables (i.e. by loading image file or capturing image with camera)
 *
 *      MBRecognizerImage* img;
 *      MBRecognizerErrorStatus status = recognizerImageCreateFromRawImage( &img, image_buffer, image_width, image_height, image_stride, RAW_IMAGE_TYPE_BGR );
 *      if (status != RECOGNIZER_ERROR_STATUS_SUCCESS) {
 *          printf("Failed to create image. Reason: %s", recognizerErrorToString(status));
 *      }
 *    @endcode
 *
 * -# Step 3b: Once you have created an image, you can perform recognition using method recognizerRunnerRecognizeFromImage.
 *    @code
 *      MBRecognizerResultState resultState = recognizerRunnerRecognizeFromImage( recognizerRunner, imageWrapper.recognizerImage, MB_FALSE, NULL );
 *
 *      if ( resultState != RECOGNIZER_RESULT_STATE_EMPTY )
 *      {
 *          // obtain results from recognizers (see Step 4)
 *      }
 *    @endcode
 *
 * -# Step 4: Obtain result structure from each of the recognizers. If some recognizer's result's state is RECOGNIZER_RESULT_STATE_VALID, then it contains recognized data.
 *      @code
 *          MBUsdlRecognizerResult result;
 *
 *          usdlRecognizerResult( &result, usdlRecognizer );
 *
 *          if ( result.baseResult.state == RECOGNIZER_RESULT_STATE_VALID )
 *          {
 *              // you can use data from the result
 *          }
 *      @endcode
 *
 * -# Finally, when done, clean the memory. Each structure has method for releasing it.
 *    @code
 *      recognizerImageDelete( &img );
 *      recognizerRunnerDelete( &recognizerRunner );
 *      usdlRecognizerDelete( &usdlRecognizer );
 *    @endcode
 *
 * @section demo Demo application
 *
 * Each platform contains its own demo application specific for that platform. Refer to its source code and documentation for more information.
 *
 * @section info Additional info
 *
 * For any inquiries, additional information or instructions please contact us at <a href="http://help.microblink.com">help.microblink.com</a>.
 * When contacting, please state which product and which platform you are using so we can help you more quickly. Also, please state that you are using
 * core components RecognizerAPI and state the version you are using. You can obtain the library version with function ::recognizerAPIGetVersionString.
 */

#ifndef RECOGNIZERAPI_H_
#define RECOGNIZERAPI_H_

#include "Recognizer/AvailableRecognizers.h"
#include "Recognizer/Licensing.h"
#include "Recognizer/Recognizer.h"
#include "Recognizer/RecognizerRunner.h"
#include "Recognizer/RecognizerImage.h"
#include "Recognizer/RecognizerError.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Returns the library version string.
 * @returns library version string
 */
MB_API char const * MB_CALL recognizerAPIGetVersionString();

/**
 * @brief Sets the location where required resources will be loaded from.
 * If not set, "res" folder within current working directory will be used.
 * NOTE: This function will not make a copy of given string, it will only save the pointer to it. Make sure the given
 * char buffer is alive during whole recognition process, since resources are lazy loaded during recognition.
 * @param resourcePath path to resource folder
 */
MB_API void MB_CALL recognizerAPISetResourcesLocation( char const * resourcePath );

#ifdef __cplusplus
}
#endif

#endif
